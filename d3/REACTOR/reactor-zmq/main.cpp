#include <assert.h>
#include <iostream>
#include <string.h>
#include <zmq.hpp>
#include <zmq_utils.h> 

#include <map>
#include <string>

#include "reactor.hpp"

using namespace ZMQ_REACTOR; 

typedef void (EVT_OP)(zmq::socket_t* s);

struct IReactorEvent
{
    virtual void operator()(zmq::socket_t* s)  = 0;
    virtual ~IReactorEvent(){};
};

void handler_a (zmq::socket_t* s)
{
    zmq::message_t query;
    s->recv (&query);
        
    const char *query_string = (const char *)query.data ();
    std::cout << "Received - handler_a: " <<  query_string << std::endl;;
    std::string r("reply from handler_a");

    zmq::message_t  resultset(r.size());
    memcpy (resultset.data (),  &r[0], r.size());
        
    s->send (resultset);
}

void handler_b (zmq::socket_t* s)
{
    zmq::message_t query;
    s->recv (&query);

    const char *query_string = (const char *)query.data ();
    std::cout << "Received - handler_b: " <<  query_string << std::endl;;

    std::string r("reply from handler_b");
    zmq::message_t  resultset(r.size());
    memcpy (resultset.data (),  &r[0], r.size());

    s->send (resultset);
}

struct ReactorEvent_A : IReactorEvent
{
    virtual void operator() (zmq::socket_t* s) 
    {
        handler_a(s);
    }
};


struct ReactorEvent_B : IReactorEvent
{
    virtual void operator() (zmq::socket_t* s) 
    {
        handler_b(s);
    }
};

void runOp(zmq::socket_t& s, zmq::socket_t& s1)
{
	reactor<EVT_OP> local_reactor;
    
	local_reactor.add(s, ZMQ_POLLIN, &handler_a);
	local_reactor.add(s1, ZMQ_POLLIN, &handler_b);
    
	while (1) 
    {
		local_reactor();
	}
}

void runInterface(zmq::socket_t& s1, zmq::socket_t& s2)
{
	reactor<IReactorEvent> local_reactor;
	
    ReactorEvent_A e1;
	local_reactor.add(s1, ZMQ_POLLIN, &e1);
	
    ReactorEvent_B e2;
	local_reactor.add(s2, ZMQ_POLLIN, &e2);
	
    while (1) 
    {
		local_reactor();
	}
}

int main (int argc, const char* argv[]) 
{
 
    try
    {
        zmq::context_t ctx (1);

        zmq::socket_t socket_1 (ctx, ZMQ_REP);
        zmq::socket_t socket_2 (ctx, ZMQ_REP);

        socket_1.bind ("tcp://lo:5555");
        socket_2.bind ("tcp://lo:5556");

        runInterface(socket_1, socket_2);
        //runOp(socket_1, socket_2);
    }
    catch (std::exception &e) 
    {
        std::cout << "An error occurred: " <<  e.what() << std::endl;
        return 1;
    }
    
    return 0;
}
