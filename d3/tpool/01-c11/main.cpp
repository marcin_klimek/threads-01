#include <iostream>
#include <vector>
#include <chrono>

#include "ThreadPool.h"
#include <boost/thread.hpp>

boost::mutex cout_mtx;

int main()
{
    
    ThreadPool pool(2);
    std::vector< std::future<int> > results;

    for(int i = 0; i < 8; ++i) {
        results.push_back(
            pool.enqueue<int>([i] {

                {
                    boost::lock_guard<boost::mutex> lock(cout_mtx);
                    std::cout << "start " << i << std::endl;
                }
                
                std::this_thread::sleep_for(std::chrono::seconds(1));

                {
                    boost::lock_guard<boost::mutex> lock(cout_mtx);
                    std::cout << "finish " << i << std::endl;
                }
                return i*i;
            })
        );
    }   

    //

    for(size_t i = 0;i<results.size();++i)
    {
        std::cout << results[i].get() << ' ';
    }
    std::cout << std::endl;
    
    return 0;
}
